const request = require('request');
const static_data = require('../update_sample');
const TXsubmitter = require('../transaction_submitter');
const token = 'vrj6p1bq151o7e0bncu3chc1ct';
let last_location = {
    latitude: 0,
    longitude: 0,
};
let location;

setInterval(() => {
    request(
        'https://app.trackimo.com/api/v3/public/devices/' + token,
        (err, response, body) => {
            if (!err && response.statusCode == 200) {
                try {
                    let data;
                    let filename =
                        __dirname + '/GPS/gps-' + Date.now() + '.json';
                    location = JSON.parse(body)[0]['geoLocation'];
                    data = static_data;
                    data[0]['updates'] = [
                        {
                            name: 'location',
                            dataType: 7,
                            value: {
                                latitude: parseInt(location.latitude * 1000000),
                                longitude: parseInt(
                                    location.longitude * 1000000,
                                ),
                            },
                        },
                    ];
                    console.log(data[0]['updates'][0]["value"]);
                    if (
                        location.latitude !== last_location.latitude ||
                        location.longitude !== last_location.longitude
                    ) {
                        last_location.latitude = location.latitude;
                        last_location.longitude = location.longitude;

                        var fs = require('fs');
                        fs.writeFile(filename, JSON.stringify(data), function(
                            err,
                        ) {
                            if (err) {
                                return console.log(err);
                            }

                            console.log('The file ' + filename + ' was saved!');
                            TXsubmitter.Submit(filename);
                        });
                    }
                } catch (err) {
                    console.log('ERROR :' + err);
                }
            }
        },
    );
}, 60000);
