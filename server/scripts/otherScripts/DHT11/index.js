// this script is for the raspberry pi

var sensor = require('node-dht-sensor');
const static_data = require('./update_sample');
const TXsubmitter = require('../../transaction_submitter');
var fs = require('fs');
var temp = 0;
var humi = 0;

setInterval(() => {
    try {
        sensor.read(11, 4, (err, temperature, humidity) => {
            if (!err) {
                console.log(`temp: ${temperature}°C, humidity: ${humidity}%`);
                var filename =
                    __dirname + '/temp-humidity/' + Date.now() + '.json';

                if (temp != temperature || humi != humidity) {
                    data = static_data;
                    data[0]['updates'] = [];

                    if (temp != temperature) {
                        data[0]['updates'].push({
                            name: 'temperature',
                            dataType: 3,
                            value: parseInt(temperature * 1000000),
                        });
                        temp = temperature;
                    }
                    if (humi != humidity) {
                        data[0]['updates'].push({
                            name: 'humidity',
                            dataType: 3,
                            value: parseInt(humidity * 1000000),
                        });
                        humi = humidity;
                    }

                    fs.writeFile(filename, JSON.stringify(data), function(err) {
                        if (err) {
                            return console.log(err);
                        }

                        console.log('The file ' + filename + ' was saved!');
                        TXsubmitter.Submit(filename);
                    });

                }
            } else console.log(err);
        });
    } catch (e) {
        console.log(e);
    }
}, 30000);
