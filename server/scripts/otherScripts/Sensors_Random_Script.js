const request = require('request');
const static_data = require('../update_sample');
//const TXsubmitter = require('../transaction_submitter');
const shell = require('shelljs');
static_data[0]['privateKey'] =
    '063f9ca21d4ef4955f3e120374f7c22272f42106c466a91d01779efba22c2cb6';
static_data[0]['recordId'] = 'shipment-854546';

setInterval(() => {
    try {
        let data;
        let filename = '/Sensors/data-' + Date.now() + '.json';
        data = static_data;
        data[0]['updates'] = [
            {
                name: 'humidity',
                dataType: 3,
                value: 5000000,
                isRelative: true,
                startValue: 52000000,
            },

            {
                name: 'temperature',
                dataType: 3,
                value: 1450000,
                isRelative: true,
                startValue: 2150000,
            },
            {
                name: 'shock',
                dataType: 3,
                value: 50000,
                isAlwaysPositive: true,
                noOpChance: 0.8,
            },
        ];
        console.log(data);
        var fs = require('fs');
        fs.writeFile(__dirname + filename, JSON.stringify(data), function(err) {
            if (err) {
                return console.log(err);
            }

            console.log('The file ' + __dirname + filename + ' was saved!');
            //TXsubmitter.Submit(__dirname + filename);
            shell.exec(
                'DATA="/otherScripts/' +
                    filename +
                    '" RATE=0 LIMIT=1 node ../run_sample_updates.js',
            );
        });
    } catch (err) {
        console.log('ERROR :' + err);
    }
}, 60000);
