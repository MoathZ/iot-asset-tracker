const request = require('request');
const static_data = require('../update_sample');
const TXsubmitter = require('../transaction_submitter');
static_data[0]['privateKey'] =
    '063f9ca21d4ef4955f3e120374f7c22272f42106c466a91d01779efba22c2cb6';
static_data[0]['recordId'] = 'shipment-854546';

setInterval(() => {
    try {
        let data;
        let filename = __dirname + '/Sensors/data-' + Date.now() + '.json';
        data = static_data;
        data[0]['updates'] = [
            {
                name: 'humidity',
                dataType: 3,
                value: 25800000,
            },

            {
                name: 'temperature',
                dataType: 3,
                value: 71450000,
            },
            {
                name: 'shock',
                dataType: 3,
                value: 50000,
            },
        ];
        console.log(data);
        var fs = require('fs');
        fs.writeFile(filename, JSON.stringify(data), function(err) {
            if (err) {
                return console.log(err);
            }

            console.log('The file ' + filename + ' was saved!');
            TXsubmitter.Submit(filename);
        });
    } catch (err) {
        console.log('ERROR :' + err);
    }
}, 60000);
